from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):

    # atributos (generales)
    email_valido = models.BooleanField(default=False)
    telefono_valido = models.BooleanField(default=False)
    es_administrador = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = "Usuarios"

    def __str__(self):
        return '%s' % (self.username)