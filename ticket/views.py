from django.views.generic import *
from ticket.models import Ticket
from django.http import JsonResponse
from ticket.forms import TicketForm

# Create your views here.

class TicketList(ListView):
    """
    ListView se refiere a una vista (lógica) para mostrar múltiples instancias de una tabla en la base de datos    
    Las vistas basadas en clases proporcionan una forma alternativa de implementar vistas como objetos de Python en lugar de funciones
    """
    model           = Ticket
    template_name   = 'ticket/list.html'

    def get_context_data(self, **kwargs):
        """
        Este método se utiliza para rellenar un diccionario y utilizarlo como contexto de plantilla
        Como su nombre indica obtiene los datos del contexto. Son los datos, variables, objectos, etc, que le vas a pasar al template para maquetarlos. 
        Por ejemplo es donde pasarías un formulario en caso de que necesites un template con 2 o más formularios.
        """
        context = super(TicketList, self).get_context_data(**kwargs)
        context['form'] = TicketForm
        return context

    def get_queryset(self):
        """
        Esta función es la que se encarga de obtener los objetos del modelo que le indicas en la variable model de tu clase. 
        Ésta función por defecto obtiene el queryset de la llamada model.objects.all(). 
        Puedes reescribir esta función de la misma manera que get_context_data() 
        para indicar alguna restrincción especial que necesites en esa view.
        """
        queryset = Ticket.objects.all()
        return queryset

class GetTicketList(View):

    http_method_names = ['get']

    def get(self, request):

        status  = True
        message = None
        data    = []

        try:
            tickets  = Ticket.objects.all()

            for ticket in tickets:
                valor = {}
                valor['id']               = ticket.id
                valor['titulo']           = ticket.titulo
                valor['estado']           = ticket.estado
                valor['descripcion']      = ticket.descripcion
                valor['tiempo_creacion']  = ticket.tiempo_creacion
                data.append(valor)
        
        except Exception as error:
            status = False
            message = str(error)

        return JsonResponse({
            'status': status,
            'message': message,
            'data': list(data)
        }, safe=False)

class TicketNew(FormView):

    """
    FormView se refiere a una vista (lógica) para mostrar y verificar un formulario Django. 
    Por ejemplo, un formulario para registrar usuarios. 
    Las vistas basadas en clases proporcionan una forma alternativa de implementar vistas como objetos de Python 
    en lugar de funciones
    """

    http_method_names = ['post']
    template_name     = 'ticket/list.html'
    form_class        = TicketForm
    success_url       = None

    def form_valid(self, form):
        try:
            status  = True
            message = 'Ticket Creado Correctamente'
            error   = None
            form.save()

        except Exception as error:
            status = False
            message = str(error)


        return JsonResponse({
            'status'  : status,
            'message' : message,
            'error'   : error
        })
        

