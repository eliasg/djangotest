from django.urls import path
from django.contrib.auth.decorators import login_required

from ticket.views import *

app_name = 'ticket'

urlpatterns = [
    path('', login_required(TicketList.as_view()), name='list'),
    path('get-tickets/', GetTicketList.as_view(), name='get_ticket'),
    path('new/', login_required(TicketNew.as_view()), name='ticket_new'),
]