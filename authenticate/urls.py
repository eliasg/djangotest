from django.urls import path

from .views import *
from django.contrib.auth import views as auth_views

app_name = 'authenticate'

urlpatterns = [

    # login
    path('login/', Login.as_view(), name='login'),
    # logout
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name="logout"),
]