from django import forms
from django.contrib.auth import authenticate
from central.models import User


class LoginForm(forms.Form):

    username = forms.CharField(
        max_length = 255,
        required = True,
        widget = forms.TextInput(
            attrs = {
                'class':'form-control',
                'placeholder': 'Usuario'
            }
        ),
        error_messages = {'required': 'Requerido'},
        label = 'usuario'
    )

    password = forms.CharField(
        required = True,
        widget = forms.PasswordInput(
            attrs = {
                'class': 'form-control',
                'placeholder': 'Contraseña'
            }
        ),
        error_messages = {'required': 'Requerido'},
        label = 'contraseña'
    )

    def clean(self):
        print('clean')
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        print(username)
        print(password)

        if username and password:

            if User.objects.filter(username=username).exists():

                auth_1 = authenticate(username=username, password=password)

                if auth_1 is not None:

                    user = authenticate(username=username, password=password)
                    if not user.is_active:
                        self.add_error('username', 'Usuario inactivo')
                else:
                    self.add_error('password', 'Contraseña inválida')
            else:
                self.add_error('username', 'Usuario no registrado')

    def login(self, request):

        username = self.cleaned_data.get('username')

        if User.objects.filter(
            username = username
            ).exists():

            user = User.objects.get(username=username)
        else:
            user = None

        return user