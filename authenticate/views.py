from django.shortcuts import render
from django.contrib.auth import login, logout
from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.views.generic import FormView

from authenticate.forms import *

# Create your views here.
class Login(FormView):

    form_class = LoginForm
    success_url = '/'
    template_name = 'auth/login.html'

    def form_invalid(self, form):
        if self.request.is_ajax():

            logout(self.request)

            return JsonResponse({
                'status': False,
                'data': {
                    'errors': form.errors
                    }
                })
        else:
            return super(Login, self).form_invalid(form)

    def form_valid(self, form):
        try:
            logout(self.request)

            if self.request.is_ajax():
                return JsonResponse({
                    'status': True,
                    'data': {}
                })
            else:
                user = form.login(self.request)
                login(self.request, user)
                return HttpResponseRedirect('/')

        except Exception as e:
            return render(self.request, '500.html', status=500)